# AIMM-Ubuntu-transition



## General

Starting on 27/06/2024, the Euler cluster has transitioned from it's old operating system , CentOS 7, to a more recent linux distribution, Ubuntu 22.04 .
Given this major change, a large part of the software available on the cluster has had - will have to be reinstalled and this causes major changes to existing workflows.
This repository exists for members of the AIMM group to coordinate and share insights as this transition is happening.

## Ubuntu vs CentOS

- [27/06/2024] On this date the default login for Euler was changed to point to Ubuntu and not CentOS. In general the following logins are available:

- Leading to Ubuntu:

euler.ethz.ch

login-beta.euler.ethz.ch

login-ubuntu.euler.ethz.ch

- Leading to CentOS:

login-centos.euler.ethz.ch


Each part of the cluster (Ubuntu and CentOS) is isolated from each other and the jobs running on each part are separate. You need to login to the part where your job is running to control it.

- [30/06/2024] CentOS will stop receiving (official) security updates after this date.

- [08/07/2024] This is the last date when the CentOS part of the cluster is guaranteed to remain available. Following this, the plan is to keep the CentOS part of the cluster alive for as long as there are no new security vulnerabilities found. There is no guarantee for the CentOS part remaining available post this date.

## Abaqus

The CentOS part of the cluster works as it used to.

The Ubuntu part only has Abaqus 2023 available at the moment. To use it run:

```
module load stack/2024-06 intel-oneapi-compilers/2023.2.0 abaqus/2023 libjpeg-turbo/3.0.0
```
- What works:
1) Implicit and explicit solver, both single and multi-core (without subroutines).
2) CAE. The libjpeg-turbo module is required for running CAE jobs with slurm.
3) Subroutines (as of 18/07/2024). Some issues still remain, like running on large numbers of cores (>8) not being possible.

- What does not work:
1) Licensing utilities. For example:
```
abaqus licensing ru
```
throws an error.

## Matlab

## LS-Dyna

## Python

There seems to be no python version available when loading intel-oneapi-compilers. The solution is to not load the intel compiler when using python.

## FAQ

- How do I tell which OS I am using?

Run
```
uname -r
```

On CentOS the output should look like:
```3.10.0-1160.118.1.el7.x86_64```

On Ubuntu the output should look like:
```5.15.0-102-generic```

- Can I view my files from Ubuntu like I did from CentOS?

Yes, the filesystems related to the HOME and SCRATCH directories are the same for both operating systems.

- How do I submit jobs on Ubuntu?

The commands for the batch submission system (slurm) such as ```sbatch```, ```sinfo``` etc. remain the same.
The installation of slurm on Ubuntu is separate though, so the output of these commands only concerns the Ubuntu part of Euler.